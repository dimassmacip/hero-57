@php
    $page = \Request::route()->getName();
@endphp

<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo">
                    <a href="{{ route('home') }}"><img class="logo-image pt-3 p-l-8" src="/cms/images/logo/logo.png" alt="Logo" srcset="/cms/images/logo/logo.png"></a>
                </div>
                <div class="toggler">
                    <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-title">Menu</li>

                <li class="sidebar-item {{ (strpos($page, 'home') !== false) ? 'active' : '' }} ">
                    <a href="{{ route('home') }}" class='sidebar-link'>
                        <i class="bi bi-house-door"></i>
                        <span>Home</span>
                    </a>
                </li>

                <li class="sidebar-item {{ (strpos($page, 'about') !== false) ? 'active' : '' }} ">
                    <a href="{{ route('about') }}" class='sidebar-link'>
                        <i class="bi bi-info-circle"></i>
                        <span>About</span>
                    </a>
                </li>

            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
        @include('components.footer')
    </div>
</div>