<footer>
    <div class="footer">
        <div class="text-center">
            <b>{{ now()->year }} &copy; QROWD</b>
        </div>
    </div>
</footer>