<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login | HERO 57</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/front/images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="/auth/fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="/auth/css/util.css">
    <link rel="stylesheet" type="text/css" href="/auth/css/main.css">
    <link rel="stylesheet" type="text/css" href="/auth/css/animated.css">
</head>

<body>
    <div class="area">
        <ul class="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        @yield('content')
    </div>
    <script src="/front/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="/auth/js/main.js"></script>
</body>
</html>