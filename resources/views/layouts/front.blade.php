<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content=" width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/front/images/favicon.png" />
    <title>HERO 57</title>
    <link href="/front/css/animate.css" rel="stylesheet" type="text/css">
    <link href="/front/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/front/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="/front/css/swiper.css" rel="stylesheet" type="text/css">
    <link href="/front/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css">
    <link href="/front/css/style.css" rel="stylesheet" type="text/css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-144098545-1');
    </script>
</head>

<body>
    <!-- Page Preloader -->
    <div id="Lfa-page-loading" class="pozo-pageloading">
        <div class="pozo-pageloading-inner"> <img src="/front/images/logo.png" class="logo" alt=""> </div>
    </div>
    <div id="pozo-page-wrapper">
        <!-- Lines -->
        <div class="content-lines-wrapper">
            <div class="content-lines-inner">
                <div class="content-lines"></div>
            </div>
        </div>
        <!-- Header -->
        <header>
            <div class="container pozo-navbar-container">
                <div class="pozo-navigation-wrap pozo-start-header start-style">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <nav class="navbar navbar-expand-md navbar-light">
                                    <!-- Logo -->
                                    <a class="navbar-brand animate-box" data-animate-effect="fadeInUp"
                                        href="{{ route('home') }}"> <img src="/front/images/logo.png" class="logo" alt=""> </a>
                                    <!-- Menu -->
                                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                        aria-expanded="false" aria-label="Toggle navigation"> <span
                                            class="navbar-toggler-icon"></span> </button>
                                    <div class="collapse navbar-collapse animate-box" data-animate-effect="fadeInUp"
                                        id="navbarSupportedContent">
                                        <ul class="navbar-nav ml-auto py-4 py-md-0">
                                            <li class="nav-item pl-md-0 ml-0 ml-md-4" id="home"> <a class="nav-link"
                                                    href="{{ route('front.home') }}">Home</a></li>
                                            <li class="nav-item pl-md-0 ml-0 ml-md-4" id="about"> <a class="nav-link"
                                                    href="{{ route('front.about') }}">About</a></li>
                                            <li class="nav-item pl-md-0 ml-0 ml-md-4"> <a
                                                    class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                                    role="button" aria-haspopup="true"
                                                    aria-expanded="false">Gallery<span
                                                        class="fa fa-angle-down"></span></a>
                                                <div class="dropdown-menu"> <a class="dropdown-item"
                                                        href="gallery.html">Product Cataloque</a> <a
                                                        class="dropdown-item" href="video.html">Video Gallery</a> </div>
                                            </li>
                                            <li class="nav-item pl-md-0 ml-0 ml-md-4"> <a class="nav-link"
                                                    href="project-page-2.html">Subohm</a></li>
                                            <li class="nav-item pl-md-0 ml-0 ml-md-4"> <a class="nav-link"
                                                    href="project-page-3.html">Qrowd</a></li>
                                            <li class="nav-item pl-md-0 ml-0 ml-md-4"> <a class="nav-link"
                                                    href="blog.html">Blog</a></li>
                                            <li class="nav-item pl-md-0 ml-0 ml-md-4"> <a class="nav-link"
                                                    href="contact.html">Contact</a> </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        @yield('content')
        <!-- Footer -->
        <footer>
            <div class="container pozo-footer-container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="pozo-social-icons">
                            <li class="pozo-social-icons-item">
                                <a class="pozo-social-link" href="index.html#"> <span class="pozo-social-icon fa fa-facebook-f"></span> </a>
                            </li>
                            <li class="pozo-social-icons-item">
                                <a class="pozo-social-link" href="index.html#"> <span class="pozo-social-icon fa fa-twitter"></span> </a>
                            </li>
                            <li class="pozo-social-icons-item">
                                <a class="pozo-social-link" href="index.html#"> <span class="pozo-social-icon fa fa-instagram"></span> </a>
                            </li>
                            <li class="pozo-social-icons-item">
                                <a href="project-page-3.html#"> <img src="/front/images/badge.png" class="img-fluid pozo-forms-img" alt=""> </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <p class="pozo-copyright">© 2020 <span>HERO 57</span> All right reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- Js -->
    <script src="/front/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="/front/js/plugins/bootstrap.min.js"></script>
    <script src="/front/js/plugins/modernizr-2.6.2.min.js"></script>
    <script src="/front/js/plugins/jquery.waypoints.min.js"></script>
    <script src="/front/js/plugins/swiper.min.js"></script>
    <script src="/front/js/plugins/jquery.fancybox.min.js"></script>
    <script src="/front/js/script.js"></script>        
</body>
</html>