<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{isset($title) ? $title : ""}} | HERO 57</title>
    <link rel="shortcut icon" href="/front/images/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="/cms/css/google-font.css">
    <link rel="stylesheet" href="/cms/css/bootstrap.css">
    <link rel="stylesheet" href="/cms/vendors/bootstrap-datatable/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/cms/vendors/bootstrap-datatable/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="/cms/vendors/bootstrap-datatable/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="/cms/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="/cms/vendors/toastify/toastify.css">
    <link rel="stylesheet" href="/cms/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="/cms/css/app.css">
    <link rel="stylesheet" href="/auth/css/util.css">
    <link rel="stylesheet" href="/cms/css/cms.css">
    @stack('css-plugins')
</head>

<body>
    <div id="app">
        @include('components.sidebar')
        <div id="main" class='layout-navbar'>
            @include('components.topbar')
            <div id="main-content">
                <div class="page-title">
                    @include('components.breadcrumbs')
                </div>
                <div class="page-heading">
                    <section class="section">
                        @yield('content')
                    </section>
                </div>
            </div>
        </div>
    </div>
    <script src="/cms/vendors/jquery/jquery.min.js"></script>
    <script src="/cms/vendors/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
    <script src="/cms/vendors/bootstrap-datatable/js/dataTables.bootstrap.min.js"></script>
    <script src="/cms/vendors/bootstrap-datatable/js/dataTables.fixedHeader.min.js"></script>
    <script src="/cms/vendors/bootstrap-datatable/js/dataTables.responsive.min.js"></script>
    <script src="/cms/vendors/bootstrap-datatable/js/responsive.bootstrap.min.js"></script>
    <script src="/cms/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/cms/vendors/toastify/toastify.js"></script>
    <script src="/cms/js/bootstrap.bundle.min.js"></script>
    @stack('js-plugins')
    <script src="/cms/js/main.js"></script>
    @if(Session::has('message'))
        <script>
            Toastify({
                text: "<?=Session::get('message')?>",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "right",
                backgroundColor: "#4fbe87",
            }).showToast();
        </script>
    @endif
    @if(Session::has('error'))
        <script>
            Toastify({
                text: "<?=Session::get('error')?>",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "right",
                backgroundColor: "#dc3545",
            }).showToast();
        </script>
    @endif
</body>

</html>
