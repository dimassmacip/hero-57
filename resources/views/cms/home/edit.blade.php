@php
    $title = "Home";
    $breadcrumbs[] = ["label" => "Home", "url" => route('home')];
    $breadcrumbs[] = ["label" => "Edit Banner", "url" => "#"];
@endphp

@extends('layouts.cms', [
    "title" => $title,
    "breadcrumbs" => $breadcrumbs,
])

@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-12 col-md-12 order-md-1 order-last">
                <h3>Edit Banner Detail</h3>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ route('home.update', [$banner->id]) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @method('PUT')
            <div class="row">
                <div class="col-md-5">
                    <div class="col-md-4">
                        <label>Image <span class="text-orange">*</span></label>
                    </div>
                    <div class="col-md-11">
                        <input type="file" name="gambar" accept="image/*" class="dropify" data-max-file-size="2M" data-height="500" data-default-file="{{ $banner->gambar_url }}" />
                        @error('gambar')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <small class="text-orange">* format file .jpg or .png, maximum size 2 MB</small>
                </div>
                <div class="col-md-7">
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>Title <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title" 
                            value="{{ $banner->title }}"
                            placeholder="Type Title">
                            @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <small class="text-orange">* Max 20 Character</small>
                    </div>
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>Subject <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input type="text" id="subject" class="form-control @error('subject') is-invalid @enderror" name="subject" 
                            value="{{ $banner->subject }}"
                            placeholder="Type Subject">
                            @error('subject')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <small class="text-orange">* Max 20 Character</small>
                    </div>
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>Short Description <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-11 form-group">
                            <textarea id="short-desc" class="form-control @error('short_desc') is-invalid @enderror" rows="2" name="short_desc">{!! $banner->short_desc !!}</textarea>
                            @error('short_desc')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <small class="text-orange">* Max 100 Character</small>
                    </div>
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>Full Description <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-11 form-group">
                            <textarea id="full-desc" class="form-control @error('full_desc') is-invalid @enderror" rows="7" name="full_desc">{!! $banner->full_desc !!}</textarea>
                            @error('full_desc')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <small class="text-orange">* Max 500 Character</small>
                    </div>
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>URL <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input type="text" id="url" class="form-control @error('url') is-invalid @enderror" name="url" 
                            value="{{ $banner->url }}"
                            placeholder="Type URL">
                            @error('url')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>Status <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <select class="form-select @error('status') is-invalid @enderror" id="status" name="status">
                                <option value="">- Select Status -</option>
                                <option value="active" {{ ($banner->status == "active") ? 'selected' : '' }}>Active</option>
                                <option value="nonactive" {{ ($banner->status == "nonactive") ? 'selected' : '' }}>Non Active</option>
                            </select>
                            @error('status')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="float-lg-end">
                <a href="{{ route('home') }}">
                    <button type="button" class="btn btn-secondary btn-lg">Cancel</button>
                </a>
                <button type="submit" class="btn btn-info btn-lg">Update</button>
            </div>
        </form>
    </div>
</div>
@endsection

@push('css-plugins')
    <link href="/cms/vendors/dropify/dist/css/dropify.min.css" rel="stylesheet">
@endpush

@push('js-plugins')
    <script src="/cms/vendors/dropify/dist/js/dropify.min.js"></script>
    <script>
        $('.dropify').dropify({
            tpl: {
                wrap: '<div class="dropify-wrapper @error('gambar') is-invalid @enderror"></div>',
            }
        });
    </script>
@endpush