@php
    $title = "Home";
    $breadcrumbs[] = ["label" => "Home", "url" => "#"];
@endphp

@extends('layouts.cms', [
    "title" => $title,
    "breadcrumbs" => $breadcrumbs,
])

@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3>Home Banner</h3>
            </div>
            <div class="col-12 col-md-6">
                <a href="{{ route('home.create') }}">
                    <button class="btn btn-warning me-1 float-end">Add Banner</button>
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row home-banner-sortable">
            @foreach ($banner as $item)
                <div class="col-md-3">
                    <div class="card preview-image">
                        <span class="label-status {{ ($item->status == 'active') ? 'active' : '' }}">{{ ucfirst($item->status) }}</span>
                        <div class="card-content image-zone" data-banner_id="{{ $item->id }}">
                            <img src="{{ $item->gambar_url }}" class="card-img-top img-fluid card-image-banner" alt="{{ $item->subject }}">
                            <div class="card-body card-cornsilk">
                                <h5 class="card-title">{{ $item->title }}</h5>
                                <p class="card-text card-text-cornsilk">
                                    {{ $item->short_desc }}
                                </p>
                                <div class="form-actions d-flex justify-content-end">
                                    <a href="{{ route('home.edit', [$item->id]) }}">
                                        <button type="button" class="btn btn-primary me-1">Update</button>
                                    </a>
                                    <a href="{{ route('home.destroy',[$item->id])}}">
                                        <button class="btn btn-danger">Delete</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@push('css-plugins')
    
@endpush

@push('js-plugins')
<script src="/cms/vendors/jquery-ui/jquery-ui.min.js"></script>
<script>
    $(function () {
        $(".home-banner-sortable").sortable({
            update: function(event, ui) {
                var order = [];
                $.each($('.image-zone'), function(index, element) {
                    order.push($(element).data('banner_id'))
                });
                
                $.ajax({
                    url: '/admin/update-sort-order',
                    method: 'post',
                    data: {
                        'order': order
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        // console.log(response);
                        location.reload();
                    }
                });
            }
        });
    });
</script>
@endpush