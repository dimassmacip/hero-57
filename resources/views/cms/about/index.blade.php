@php
    $title = "About";
    $breadcrumbs[] = ["label" => "About", "url" => "#"];
@endphp

@extends('layouts.cms', [
    "title" => $title,
    "breadcrumbs" => $breadcrumbs,
])

@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-12 col-md-12 order-md-1 order-first">
                <h3>About Us</h3>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="#" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row pb-4">
                <div class="col-md-5">
                    <div class="col-md-4">
                        <label>Image <span class="text-orange">*</span></label>
                    </div>
                    <div class="col-md-11">
                        <input type="file" name="gambar" accept="image/*" class="dropify" data-max-file-size="2M" data-height="500"  />
                        @error('gambar')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <small class="text-orange">* format file .jpg or .png, maximum size 2 MB</small>
                </div>
                <div class="col-md-7">
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>Title <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Type Title" value="{{ old('title') }}">
                            @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <small class="text-orange">* Max 20 Character</small>
                    </div>
                    <div class="pb-4">
                        <div class="col-md-4">
                            <label>Full Description <span class="text-orange">*</span></label>
                        </div>
                        <div class="col-md-11 form-group">
                            <textarea id="full-desc" class="form-control @error('full_desc') is-invalid @enderror" rows="7" name="full_desc">{{ old('full_desc') }}</textarea>
                            @error('full_desc')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <small class="text-orange">* Max 500 Character</small>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5>Partner Logo <span class="text-orange">*</span></h5>
                <input type="file" id="pro-image" name="filename[]" accept="image/*" class="dropify" data-max-file-size="2M" multiple />
                <small class="text-orange">* format file .jpg or .png, maximum size 2 MB</small>
                <div class="preview-images-zone"></div>
            </div>
            <hr>
            <div class="float-lg-end">
                <a href="{{ route('about') }}">
                    <button type="button" class="btn btn-secondary btn-lg">Cancel</button>
                </a>
                <button type="submit" class="btn btn-info btn-lg">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection

@push('css-plugins')
    <link href="/cms/vendors/dropify/dist/css/dropify.min.css" rel="stylesheet">
@endpush

@push('js-plugins')
    <script src="/cms/vendors/dropify/dist/js/dropify.min.js"></script>
    <script src="/cms/vendors/jquery-ui/jquery-ui.min.js"></script>
    <script>
        $('.dropify').dropify({
            tpl: {
                wrap: '<div class="dropify-wrapper @error('gambar') is-invalid @enderror"></div>',
            }
        });
    </script>
    <script>
        $(document).ready(function () {
            document.getElementById('pro-image').addEventListener('change', readImage, false);
    
            $(".preview-images-zone").sortable();
    
            $(document).on('click', '.image-cancel', function (ev) {
                let no = $(this).data('no');
                alertify
                    .okBtn("Ya")
                    .cancelBtn("Tidak")
                    .confirm("Apakah Anda yakin ingin menghapus data tersebut?", function (ev) {
                        ev.preventDefault();
                        $(".preview-image.preview-show-" + no).remove();
                    }, function (ev) {
                        ev.preventDefault();
                    });
            });
    
            $(document).on('click', '#alertify-confirm', function (ev) {
                alertify
                    .okBtn("Ya")
                    .cancelBtn("Tidak")
                    .confirm("Apakah Anda yakin ingin mengubah data tersebut?", function (ev) {
                        ev.preventDefault();
                        $("#pengaturan-form").submit();
                    }, function (ev) {
                        ev.preventDefault();
                    });
            });
        });
    
        var num = 1;
    
        function readImage() {
            if (window.File && window.FileList && window.FileReader) {
                var files = event.target.files;
                var output = $(".preview-images-zone");
                output.css("display", "block");
    
                for (let i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (!file.type.match('image')) continue;
    
                    var picReader = new FileReader();
    
                    picReader.addEventListener('load', function (event) {
                        var picFile = event.target;
                        var html = '<div class="preview-image preview-show-' + num + '">' +
                            '<div class="image-cancel" data-no="' + num + '"> x </div>' +
                            '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result +
                            '">' + '<input type="hidden" name="order[]" value="'+ num +'">'
                            + '<input type="hidden" name="new_order[]" value="'+ num +'">'
                            + '<input type="hidden" name="new_filename[]" value="'+ event.target.result +'">'
                            + '</div>';
    
                        output.append(html);
                        num = num + 1;
                    });
    
                    picReader.readAsDataURL(file);
                }
            } else {
                console.log('Browser not support');
            }
        }
    
    </script>
@endpush