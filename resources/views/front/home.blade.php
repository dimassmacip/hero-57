@extends('layouts.front')

@section('content')
<div class="container">
    <section class="pozo-section-slider pt-130">
        <div class="next-container-center">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="swiper-slide-block">
                            <div class="swiper-slide-block-img animate-box" data-animate-effect="fadeInLeft" data-swiper-parallax-y="70%">
                                <a href="project-page.html"> <img src="/front/images/slider/1.jpg" alt=""> </a>
                            </div>
                            <div class="swiper-slide-block-text animate-box" data-animate-effect="fadeInRight">
                                <h2 data-swiper-parallax-x="-60%" class="next-main-title">HERO 57</h2>
                                <h3 data-swiper-parallax-x="-50%" class="next-main-subtitle">Vape, e-liquid</h3>
                                <p data-swiper-parallax-x="-40%" class="next-paragraph">Quisque pellentesque odio ut libero iaculis, nec fringilla sapien tincidunt. Sed laoreet pulvinar ex sed egestas. Vestibulum pharetra enim the dui tempus in blandit nulla egestas.</p> <a data-swiper-parallax-x="-30%" style="z-index: 5;" class="next-link" href="project-page.html">Discover</a> <span data-swiper-parallax-y="60%" class="next-number">1</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-slide-block">
                            <div class="swiper-slide-block-img" data-swiper-parallax-y="70%">
                                <a href="project-page-2.html"><img src="/front/images/slider/2.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide-block-text">
                                <h2 data-swiper-parallax-x="-60%" class="next-main-title">SUBOHM 3.0</h2>
                                <h3 data-swiper-parallax-x="-50%" class="next-main-subtitle">Vape Store & Eatery</h3>
                                <p data-swiper-parallax-x="-40%" class="next-paragraph">Quisque pellentesque odio ut libero iaculis, nec fringilla sapien tincidunt. Sed laoreet pulvinar ex sed egestas. Vestibulum pharetra enim the dui tempus in blandit nulla egestas.</p> <a data-swiper-parallax-x="-30%" class="next-link" href="project-page-2.html">Discover</a> <span data-swiper-parallax-y="60%" class="next-number animate-box" data-animate-effect="fadeInUp">2</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-slide-block">
                            <div class="swiper-slide-block-img" data-swiper-parallax-y="70%">
                                <a href="project-page-3.html"> <img src="/front/images/slider/3.jpg" alt=""> </a>
                            </div>
                            <div class="swiper-slide-block-text">
                                <h2 data-swiper-parallax-x="-60%" class="next-main-title">Qrowd App</h2>
                                <h3 data-swiper-parallax-x="-50%" class="next-main-subtitle">Anti Counterfeit System</h3>
                                <p data-swiper-parallax-x="-40%" class="next-paragraph">Quisque pellentesque odio ut libero iaculis, nec fringilla sapien tincidunt. Sed laoreet pulvinar ex sed egestas. Vestibulum pharetra enim the dui tempus in blandit nulla egestas.</p> <a data-swiper-parallax-x="-30%" class="next-link" href="project-page-3.html">Discover</a> <span data-swiper-parallax-y="60%" class="next-number">3</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next animate-box" data-animate-effect="fadeInRight"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </div>
                <div class="swiper-button-prev animate-box" data-animate-effect="fadeInLeft"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> </div>
            </div>
        </div>
    </section>
</div>
@endsection