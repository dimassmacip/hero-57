@extends('layouts.front')

@section('content')
<!-- About Us -->
<section class="about pt-130">
    <div class="container">
        <div class="row">
            <div class="col-md-4 image animate-box" data-animate-effect="fadeInUp">
                <div class="img"> <img src="/front/images/about.jpg" alt=""> </div>
            </div>
            <div class="col-md-8 image animate-box" data-animate-effect="fadeInUp">
                <!-- About -->
                <div class="row">
                    <div class="col-md-12">
                        <h3>Welcome to <span>Hero 57</span></h3>
                        <p>Curabitur mollis tortor ac congue porta. Donec vel eros rhoncus nulla posuere interdum vitae
                            nec lectus. Aenean rutrum the tortor, at tincidunt arcu malesuada ultricies. Praesent eu
                            aliquam ipsum. Sed at turpis vitae ex commodo fermentum vitae dapibus libero. Fusce orci
                            orci, blandit ut fermentum sed, eleifend vitae eros. Phasellus id justo sagittis, sagittis
                            quam the in porttitor enim. Sed at sapien nec libero pharetra tincidunt vitae a ipsum.</p>
                        <p>Donec arcu lectus, bibendum sed turpis ut, porta dictum leo. Quisque tincidunt ante et est
                            malesuada volutpat. Praesent ultrices mi ut nunc volutpat tempus. Cras vitae nibh in neque
                            cursus finibus ac et metus. Nulla at euismod sem. Morbi vitae eros orci. Quisque tincidunt
                            ante et est malesuada volutpat.</p> <img src="/front/images/sign.png" class="pozo-about-signature"
                            alt="">
                        <p><b>Phone:</b> 0813-1778-3087
                            <br /><b>E-mail:</b> info@hero57.com

                    </div>
                </div>
                <!-- Team -->
                <div class="row mt-60">
                    <div class="col-md-12">
                        <h3>Core <span>Team</span></h3>
                        <br />
                    </div>
                    <div class="col-md-6">
                        <div class="team-holder">
                            <div class="team-image-holder"> <img src="/front/images/team/01.jpg" class="img-fluid"
                                    alt="">
                                <div class="team-side-info">
                                    <h4 class="team-name">Reza Pahlawan </h4>
                                    <h6 class="team-position">Founder, CEO</h6>
                                </div>
                            </div>
                        </div>
                        <ul class="team-info-social text-center">
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="team-holder">
                            <div class="team-image-holder"> <img src="/front/images/team/02.jpg" class="img-fluid"
                                    alt="">
                                <div class="team-side-info">
                                    <h4 class="team-name">Vito Taruna </h4>
                                    <h6 class="team-position">Financial Director</h6>
                                </div>
                            </div>
                        </div>
                        <ul class="team-info-social text-center">
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="team-holder">
                            <div class="team-image-holder"> <img src="/front/images/team/01.jpg" class="img-fluid"
                                    alt="">
                                <div class="team-side-info">
                                    <h4 class="team-name">Martin Bong </h4>
                                    <h6 class="team-position">Marketing Director</h6>
                                </div>
                            </div>
                        </div>
                        <ul class="team-info-social text-center">
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="team-holder">
                            <div class="team-image-holder"> <img src="/front/images/team/01.jpg" class="img-fluid"
                                    alt="">
                                <div class="team-side-info">
                                    <h4 class="team-name">Stephanus Benjamin</h4>
                                    <h6 class="team-position">Prod. Director</h6>
                                </div>
                            </div>
                        </div>
                        <ul class="team-info-social text-center">
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection