<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class About extends BaseModel
{
    use HasFactory;

    protected $table = "about";

    public function rules()
    {
        return [
            "title" => "required|max:20",
            "full_desc" => "required|max:500",
            "gambar" => "required"
        ];
    }
}
