<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Banner extends BaseModel
{
    use HasFactory;

    protected $table = "banner";
    protected $appends = ["gambar_url"];

    public function rules()
    {
        switch ($this->request->method()) {
            case 'POST':
                return [
                    "title" => "required|max:20",
                    "subject" => "required|max:20",
                    "short_desc" => "required|max:100",
                    "full_desc" => "required|max:500",
                    "url" => "required",
                    "status" => "required",
                    "gambar" => "required"
                ];
                break;
            case 'PUT':
                return [
                    "title" => "required|max:20",
                    "subject" => "required|max:20",
                    "short_desc" => "required|max:100",
                    "full_desc" => "required|max:500",
                    "url" => "required",
                    "status" => "required",
                ];
                break;            
            default:
                return [
                    "title" => "required|max:20",
                    "subject" => "required|max:20",
                    "short_desc" => "required|max:100",
                    "full_desc" => "required|max:500",
                    "url" => "required",
                    "status" => "required",
                    "gambar" => "required"
                ];
                break;
        }
    }

    public function message()
    {   
        return [
            "title.required" => "Title field is required.",
            "title.max" => "Title field max 20 character.",
            "subject.required" => "Subject field is required.",
            "subject.max" => "Subject field max 20 character.",
            "short_desc.required" => "Short Desc field is required.",
            "full_desc.required" => "Full Desc field is required.",
            "url.required" => "URL field is required.",
            "status.required" => "Status field is required.",
            "gambar.required" => "Image field is required."
        ];
    }

    public function getGambarUrlAttribute()
    {
        if (empty($this->gambar)) {
            return "/public/cms/images/samples/no-image.png";
        } else {
            return Storage::url($this->gambar);
        }
    }

    public function getShortDescAttribute($value)
    {
        $string = strip_tags($value);
        if (strlen($string) > 100) {
            $stringCut = substr($string, 0, 100);
            $endPoint = strrpos($stringCut, ' ');
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }
        return $string;
    }
}
