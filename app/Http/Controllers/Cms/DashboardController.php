<?php

namespace App\Http\Controllers\Cms;

use App\Models\Banner;
use Illuminate\Http\Request;
use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $banner = Banner::orderBy('order')->get();
        return view('cms.home.index', [
            "banner" => $banner
        ]);
    }

    public function create()
    {
        return view('cms.home.create');
    }

    public function store(Request $request)
    {
        $model = new Banner($request->all());
        $model->validate();
        if ($model->fails) {
            return redirect(route('home.create'))
            ->withErrors($model->errors)
            ->withInput();
        }

        if($request->hasFile('gambar'))
        {
            $images = $request->file('gambar');
            $path = ImageHelper::uploadAndResize($images, 'home-banner', 300);
        }

        $model->gambar = isset($path) ? $path : null;
        $model->title = $request->title;
        $model->subject = $request->subject;
        $model->short_desc = $request->short_desc;
        $model->full_desc = $request->full_desc;
        $model->status = $request->status;
        $model->save();

        return redirect(route('home'))->with("message", "Berhasil Simpan");
    }

    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('cms.home.edit', [
            "banner" => $banner
        ]);
    }

    public function update(Request $request, $id)
    {
        $model = new Banner($request->all());
        $model->validate();
        if ($model->fails) {
            return redirect(route('home.edit', [$id]))
            ->withErrors($model->errors)
            ->withInput();
        }

        $banner = $model->find($id);
        if($request->hasFile('gambar'))
        {
            ImageHelper::removeFilesFromDirectories($banner->gambar);
            $images = $request->file('gambar');
            $path = ImageHelper::uploadAndResize($images, 'home-banner', 300);
        }
        $banner->gambar = isset($path) ? $path : $banner->gambar;
        $banner->title = $request->title;
        $banner->subject = $request->subject;
        $banner->short_desc = $request->short_desc;
        $banner->full_desc = $request->full_desc;
        $banner->status = $request->status;
        $banner->save();

        return redirect(route('home'))->with("message", "Berhasil Update");
    }

    public function destroy($id)
    {
        $banner = Banner::find($id);
        ImageHelper::removeFilesFromDirectories($banner->gambar);
        $banner->delete();
        return redirect(route('home'))->with("message", "Berhasil Hapus Data");
    }

    public function updateSortOrder(Request $request)
    {
        try {
            $orderStart = 1;
            if (isset($request->order)) {
                foreach ($request->order as $index => $id) {
                    $order = $orderStart + $index;
                    Banner::where([
                        'id' => $id
                    ])
                    ->update(['order' => $order]);
                }
                return true;
            }
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}
