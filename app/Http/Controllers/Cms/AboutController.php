<?php

namespace App\Http\Controllers\Cms;

use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        return view('cms.about.index');
    }
}
