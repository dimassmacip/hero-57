<?php

namespace App\Helpers;

use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class ImageHelper
{
    public static $dir = 'public';
    public static $parent;
    public static $filename;

    public static function uploadFile($file, $dir = null)
    {
        $uploadFolder = Self::setDir($dir);
        $filename = Self::createFilename($file);
        $path = $file->storeAs(
            $uploadFolder, $filename, 'public'
        );
        
        $visibility = Storage::getVisibility('public/', $path);
        Storage::setVisibility('public/' . $path, 'public');
        return $path;
    }
    public static function uploadAndResize($file, $dir = null, $size=1280)
    {
        $filename = Self::createFilename($file);
        $thumb = Image::make($file)->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode($file->extension());
        $name = $dir."/".$filename;
        Storage::disk("public")->put($name, $thumb->__toString());
        return $name;
    }

    public static function uploadBase64File($base64_image, $dir = null)
    {        
        if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
            $filename = Self::createFilenameBase64($base64_image);
            $data = substr($base64_image, strpos($base64_image, ',') + 1);
            $data = base64_decode($data);
            
            $thumb = Image::make($data)->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode("jpg");
            $data = $thumb->__toString();
            $path = "";
            try {
                $im = imagecreatefromstring($data);
                $path = "pengaturan/webpImg".time().".webp";
                $ph = imagewebp($im, "storage/".$path);
                if (!$ph) {
                    if ( Storage::disk('local')->put('public/pengaturan/' . $filename, $data) ) {
                        $path = 'pengaturan/' . $filename;  
                    } 
                }
            } catch (\Throwable $th) {
                if ( Storage::disk('local')->put('public/pengaturan/' . $filename, $data) ) {
                    $path = 'pengaturan/' . $filename;  
                } 
            }

            return $path;
        }
    }

    public static function createFilenameBase64($base64_image) 
    {
        $img = preg_replace('/^data:image\/\w+;base64,/', '', $base64_image);
        $type = explode(';', $base64_image)[0];
        $type = explode('/', $type)[1];
        $filename = Str::random() .'-pengaturan.' . $type;
        return $filename;
    }

    public static function createFilename($file) 
    {
        $filename = strtotime(date('Y-F-d H:i:s')) .'-'. $file->getClientOriginalName();
        return $filename;
    }

    /**
     * Set Directory
     * @param Self
     */
    public static function setDir($dir = null)
    {
        return (empty($dir)) ? Self::$dir : $dir ;
    }

    public static function removeFilesFromDirectories($file) 
    {
        Storage::delete('public/' . $file);
        return true;
    }

    public static function getInfo($file)
    {
        $img = Image::make($file);
        return [
            'size'         => strlen($img->encoded),
            'width'        => $img->width(),
            'height'       => $img->height(),
            'mime_type'    => $img->mime(),
            'content_type' => $img->mime(),
        ];
    }
}
