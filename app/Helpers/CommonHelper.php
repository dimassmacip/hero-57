<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use PhpOffice\PhpSpreadsheet\Calculation\Web;

class CommonHelper
{
    public  static function snakeToWords(String $input)
    {
        $snake = explode("_", $input);
        return ucwords(implode(' ', $snake));
    }
    public  static function slugToWords(String $input)
    {
        $snake = explode("-", $input);
        return ucwords(implode(' ', $snake));
    }

    public  static function currency($input, bool $useRp = false, bool $alignRp = false){
        $suffix = $useRp ? "Rp. " : "";
        if ($alignRp) {
            return $suffix.'<span class="float-right">'.number_format($input, 0, ",", ".").'</span> ';
        }
        return $suffix. number_format($input, 0, ",", ".");
    }

    public  static function getPronounceThousands($num)
    {
        $suffix = "";
        if ($num>1000000000) {
            $num = $num/1000000000;
            $suffix = " T";
        }elseif ($num>1000000) {
            $num = $num/1000000;
            $suffix = " M";
        } elseif($num > 1000) {
            $num = $num/1000;
            $suffix = " K";
        }
        $str = number_format($num, 2, ",", ".");
        return $str.$suffix;
    }
    public static function getDp()
    {
        if (Auth::check() && Auth::user()->name) {
            $name = Auth::user()->name;
            $arr = explode(" ", $name);
            return (isset($arr[1]) ? ucfirst($arr[0][0].$arr[1][0]) : ucfirst($arr[0][0]));
        } else {
            return "-";
        }
    }
}

