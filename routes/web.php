<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('front.home');
Route::get('/about', [App\Http\Controllers\HomeController::class, 'about'])->name('front.about');
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::middleware(["auth"])->group(function(){
    Route::prefix("admin")->group(function(){
        
        // Home Banner
        Route::get('/home', [App\Http\Controllers\Cms\DashboardController::class, 'index'])->name('home');
        Route::get('/home/create', [App\Http\Controllers\Cms\DashboardController::class, 'create'])->name('home.create');
        Route::get('/home/edit/{id}', [App\Http\Controllers\Cms\DashboardController::class, 'edit'])->name('home.edit');
        Route::get('/home/destroy/{id}', [App\Http\Controllers\Cms\DashboardController::class, 'destroy'])->name('home.destroy');
        Route::put('/home/update/{id}', [App\Http\Controllers\Cms\DashboardController::class, 'update'])->name('home.update');
        Route::post('/home/store', [App\Http\Controllers\Cms\DashboardController::class, 'store'])->name('home.store');
        Route::post('/update-sort-order', [App\Http\Controllers\Cms\DashboardController::class, 'updateSortOrder']);

        // About
        Route::get('/about', [App\Http\Controllers\Cms\AboutController::class, 'index'])->name('about');
    });
});
